#include "postgres.h"
#include "miscadmin.h"
#include "fmgr.h"
#include "utils/rel.h"
#include "utils/array.h"
#include "utils/builtins.h"
#include "executor/spi.h"
#include "catalog/pg_type.h"
#include "utils/timestamp.h"
#include "commands/trigger.h"
#include "access/htup_details.h"
#include "utils/guc.h"
#include "utils/lsyscache.h"
#include "utils/datum.h"
#include "math.h"
#include "utils/reltrigger.h"

#ifdef PG_MODULE_MAGIC
PG_MODULE_MAGIC;
#endif // PG_MODULE_MAGIC

/*
 *  Number of bind parameters and bind positions within the
 *  audit_event_insert query. If you manipulate bindpoints or add query params
 *  be sure that these are accurate and that the snprintf that dynamically
 *  generates the final audit insert query is accurate. These parameters
 *  modify memory allocation and array walks.
 */
#define PARAM_COUNT_PER_ROW 6 // Number of bindpoints
#define AUDIT_FIELD_POSITION 0
#define PK_COLS_POSITION 1
#define UID_POSITION 2
#define ROW_OP_POSITION 3
#define OLD_VALUE_POSITION 4
#define NEW_VALUE_POSITION 5

// Array used for bulk memory initialization of Oid array
const Oid cstring_oid_array[PARAM_COUNT_PER_ROW] = {
        CSTRINGOID,
        CSTRINGOID,
        CSTRINGOID,
        CSTRINGOID,
        0,
        0
};

// Note that the casts are mandatory. Parameters are bound in as CSTRINGs and
// are not directly coercible to the target datatype, so VARCHAR is used as an
// intermediate type

// Single attribute insert query
const char * audit_event_insert = "\
INSERT INTO cyanaudit.%s \
            ( \
                audit_field, \
                recorded, \
                txid, \
                pk_vals, \
                uid, \
                row_op, \
                audit_transaction_type, \
                old_value, \
                new_value \
            ) \
     VALUES \
            ( \
                $1::VARCHAR::INTEGER, \
                clock_timestamp(), \
                txid_current(), \
                $2::VARCHAR::VARCHAR[], \
                $3::VARCHAR::INTEGER, \
                $4::CHAR(1), \
                NULLIF( \
                    current_setting( \
                        'cyanaudit.audit_transaction_type', \
                        true \
                    ), \
                    '' \
                )::INTEGER, \
                $5::VARCHAR, \
                $6::VARCHAR \
            )";

// query fragment for additional attributes that are logable
const char * audit_event_insert_addl_row = "\
            ,(\
                $%u::VARCHAR::INTEGER, \
                clock_timestamp(), \
                txid_current(), \
                $%u::VARCHAR::VARCHAR[], \
                $%u::VARCHAR::INTEGER, \
                $%u::CHAR(1), \
                NULLIF( \
                    current_setting( \
                        'cyanaudit.audit_transaction_type', \
                        true \
                    ), \
                    '' \
                )::INTEGER, \
                $%u::VARCHAR, \
                $%u::VARCHAR \
            )";

// Lookup the partition the insert will end up at rather than incurring a
// penalty from calling fn_redirect_audit_events
const char * partition_lookup = "\
    SELECT regexp_replace( \
               encode( t.tgargs, 'escape' ), \
               '\\\\.*$', \
               '' \
           ) AS partition_name \
      FROM pg_trigger t \
INNER JOIN pg_class c \
        ON c.oid = t.tgrelid \
       AND c.relname = 'tb_audit_event' \
INNER JOIN pg_namespace n \
        ON n.nspname = 'cyanaudit' \
       AND n.oid = c.relnamespace \
     WHERE t.tgname = 'tr_redirect_audit_events'";

const char * config_lookup = "\
    SELECT value \
      FROM cyanaudit.tb_config \
     WHERE name = $1::VARCHAR";

const char * uid_lookup = "\
    SELECT %s \
      FROM %s \
     WHERE ( %s )::VARCHAR = $1::VARCHAR";

const char * update = "U";
const char * insert = "I";
const char * delete = "D";

const char * set_txid = "\
    SELECT set_config( 'cyanaudit.last_txid', txid_current()::TEXT, false )";
const char * pk_cols_prefix = "{ ";
const char * pk_cols_suffix = " }";

static Datum * parse_input_array( char *, int * );
static char * get_current_uid( void );
static void set_default_uid( void );
static char * populate_pk_cols(
    unsigned int,
    TupleDesc,
    HeapTuple,
    Datum *
);
static inline Datum get_return_tuple( TriggerData * );

PG_FUNCTION_INFO_V1( fn_log_audit_event );

Datum fn_log_audit_event( PG_FUNCTION_ARGS )
{
    TriggerData *   trigger_data     = NULL;
    TupleDesc       tuple            = NULL;
    HeapTuple       return_tuple     = NULL;
    HeapTuple       new              = NULL;
    HeapTuple       old              = NULL;
    HeapTuple       htup             = NULL;
    SPITupleTable * tuptable         = NULL;
    Datum *         params           = NULL;
    Datum *         dat_pk_cols      = NULL;
    Datum *         dat_audit_fields = NULL;
    Datum *         dat_column_names = NULL;
    Datum           new_value        = {0};
    Datum           old_value        = {0};
    int             num_pk_cols      = 0;
    int             num_audit_fields = 0;
    int             num_column_names = 0;
    int             column_num       = 0;
    int             spi_ret          = 0;
    bool            new_is_null      = false;
    bool            old_is_null      = false;
    bool            type_byval       = false;
    char *          audit_field      = NULL;
    char *          column_name      = NULL;
    char *          pk_cols          = NULL;
    char *          nulls            = NULL;
    char *          query            = NULL;
    unsigned int    i                = 0;
    unsigned int    j                = 0;
    unsigned int    index_offset     = 0;
    unsigned int    params_count     = 0;
    unsigned int    row_count        = 0;
    unsigned int    query_len        = 0;
    unsigned int    old_query_len    = 0;
    unsigned int    param_index      = 0;
    int16           type_len         = 0;
    Oid             typeid           = {0};
    Oid *           param_types      = NULL;
    char *          target           = NULL;
    char *          uid              = NULL;
    char *          enabled          = NULL;

    trigger_data = ( TriggerData * ) fcinfo->context;

    // Prevent invalid calls to function.
    if( !CALLED_AS_TRIGGER( fcinfo ) )
    {
        elog(
            ERROR,
            "fn_log_audit_event: cannot be called as a standalone function"
        );
    }
    else if( !TRIGGER_FIRED_AFTER( trigger_data->tg_event ) )
    {
        elog(
            ERROR,
            "fn_log_audit_event: must be called after DML"
        );
    }
    else if( !TRIGGER_FIRED_FOR_ROW( trigger_data->tg_event ) )
    {
        elog(
            ERROR,
            "fn_log_audit_event: must be called for each row"
        );
    }

    // Check if cyanaudit is globally enabled
    enabled = ( char * ) GetConfigOption(
        "cyanaudit.enabled",
        true,
        false
    );

    if(
           enabled == NULL
        || strlen( enabled ) == 0
        || strncmp( enabled, "0", 1 ) == 0
        || strncmp( enabled, "f", 1 ) == 0
        || strncmp( enabled, "false", 5 ) == 0
      )
    {
        return get_return_tuple( trigger_data );
    }

    // Switch memory contexts
    spi_ret = SPI_connect();

    if( unlikely( spi_ret < 0 ) )
        elog( ERROR, "Failed to connect to SPI" );

    /*
     *  Parse out target partition from tg_trigger, saving
     *  a call to the fn_redirect_audit_events BEFORE trigger
     */
    spi_ret = SPI_execute( partition_lookup, true, 1 );

    if( likely( SPI_tuptable != NULL && spi_ret == SPI_OK_SELECT ) )
    {
        tuptable = SPI_tuptable;
        htup = tuptable->vals[0];

        if( likely( htup != NULL ) )
        {
            target = SPI_getvalue(
                htup,
                SPI_tuptable->tupdesc,
                1
            );
        }

        SPI_freetuptable( SPI_tuptable );
        SPI_tuptable = NULL;
    }

    if( unlikely( target == NULL ) )
    {
        // IFF not found fallback to parent table and
        // let fn_redirect_audit_events handle it
        target = "tb_audit_event";
    }

    uid = get_current_uid();

    /* Input processing frontend */
    if( unlikely( trigger_data->tg_trigger->tgnargs != 3 ) )
    {
        elog(
            WARNING,
            "fn_log_audit_event: Invalid argument count"
        );

        SPI_finish();
        return get_return_tuple( trigger_data );
    }

    dat_pk_cols = parse_input_array(
        trigger_data->tg_trigger->tgargs[0],
        &num_pk_cols
    );
    dat_audit_fields = parse_input_array(
        trigger_data->tg_trigger->tgargs[1],
        &num_audit_fields
    );
    dat_column_names = parse_input_array(
        trigger_data->tg_trigger->tgargs[2],
        &num_column_names
    );

    if( unlikely( num_column_names != num_audit_fields ) )
    {
        elog(
            WARNING,
            "fn_log_audit_event: Mismatch on audit"
            " field input and column name inputs"
        );
        SPI_finish();
        return get_return_tuple( trigger_data );
    }

    if( unlikely( num_column_names <= 0 ) )
    {
        elog(
            WARNING,
            "fn_log_audit_event: No loggable columns provided to function"
        );
        SPI_finish();
        return get_return_tuple( trigger_data );
    }

    // Take two bytes for the '%s', add a byte for the null term
    query_len = strlen( audit_event_insert ) - 1 + strlen( target );
    query = ( char * ) SPI_palloc(
        sizeof( char ) * query_len
    );

    if( unlikely( query == NULL ) )
    {
        elog(
            WARNING,
            "fn_log_audit_event: Failed to allocate memory"
        );
        SPI_finish();
        return get_return_tuple( trigger_data );
    }

    param_index = PARAM_COUNT_PER_ROW;

    if(
        unlikely(
             num_column_names <= 0
          || num_column_names != num_audit_fields
          || query == NULL
        )
      )
    {
        SPI_finish();
        return get_return_tuple( trigger_data );
    }

    // Preallocate the arrays to the maximum possible length. This is somewhat
    // wasteful but prevents realloc calls later in the case of an undershoot
    params = ( Datum * ) SPI_palloc(
        sizeof( Datum ) * PARAM_COUNT_PER_ROW * num_column_names
    );

    nulls = ( char * ) SPI_palloc(
        sizeof( char ) * ( PARAM_COUNT_PER_ROW * num_column_names + 1 )
    );

    param_types = ( Oid * ) SPI_palloc(
        sizeof( Oid ) * PARAM_COUNT_PER_ROW * num_column_names
    );

    if( unlikely( params == NULL || nulls == NULL || param_types == NULL ) )
    {
        elog(
            WARNING,
            "fn_log_audit_event: Failed to allocate memory"
        );

        if( params != NULL )
            SPI_pfree( params );

        if( nulls != NULL )
            SPI_pfree( nulls );

        if( param_types != NULL )
            SPI_pfree( param_types );

        SPI_finish();
        return get_return_tuple( trigger_data );
    }

    // Bounding memory initialization of OID array. 2/3 are CSTRINGOID,
    // so we initialize that and have a NULL mask for new/old Datums
    for(
         i = 0;
         i < num_column_names * PARAM_COUNT_PER_ROW;
         i += PARAM_COUNT_PER_ROW
       )
    {
        memcpy(
            param_types + i,
            cstring_oid_array,
            PARAM_COUNT_PER_ROW * sizeof( Oid )
        );
    }

    memset( nulls, ' ', PARAM_COUNT_PER_ROW * num_column_names );

    tuple = trigger_data->tg_relation->rd_att;

    snprintf(
        query,
        query_len,
        audit_event_insert,
        target
    );

    query[query_len - 1] = '\0';

    if( TRIGGER_FIRED_BY_UPDATE( trigger_data->tg_event ) )
    {
        HeapTupleHeader new_header = NULL;
        HeapTupleHeader old_header = NULL;

        return_tuple = trigger_data->tg_newtuple;
        new          = trigger_data->tg_newtuple;
        old          = trigger_data->tg_trigtuple;
        new_header   = new->t_data;
        old_header   = old->t_data;

        // Compare tuples to verify a no-change update hasn't happened
        if(
               new->t_len == old->t_len
            && new_header->t_hoff == old_header->t_hoff
            && HeapTupleHeaderGetNatts( new_header ) ==
               HeapTupleHeaderGetNatts( old_header )
            && ( new_header->t_infomask & ~HEAP_XACT_MASK ) ==
               ( old_header->t_infomask & ~HEAP_XACT_MASK )
            && memcmp(
                   (( char * ) new_header ) + SizeofHeapTupleHeader,
                   (( char * ) old_header ) + SizeofHeapTupleHeader,
                   new->t_len - SizeofHeapTupleHeader
               ) == 0
          )
        {
            SPI_finish();
            return PointerGetDatum( NULL );
        }

        pk_cols = populate_pk_cols(
            num_pk_cols,
            tuple,
            new,
            dat_pk_cols
        );

        if( unlikely( pk_cols == NULL ) )
        {
            SPI_finish();
            return PointerGetDatum( return_tuple );
        }

        for( i = 0; i < num_column_names; i++ )
        {
            column_name = TextDatumGetCString( dat_column_names[i] );
            audit_field = TextDatumGetCString( dat_audit_fields[i] );

            column_num = SPI_fnumber(
                tuple,
                column_name
            );

            if(
                 unlikely(
                       column_num <= 0
                    || column_num == SPI_ERROR_NOATTRIBUTE
                 )
              )
            {
                elog(
                    WARNING,
                    "fn_log_audit_event: Attribute %s not found in record",
                    column_name
                );
                SPI_finish();
                return PointerGetDatum( return_tuple );
            }

            old_value = SPI_getbinval(
                old,
                tuple,
                column_num,
                &old_is_null
            );

            new_value = SPI_getbinval(
                new,
                tuple,
                column_num,
                &new_is_null
            );

            if( new_is_null && old_is_null )
                continue;

            typeid = SPI_gettypeid( tuple, column_num );
            get_typlenbyval( typeid, &type_len, &type_byval );

            if(
                   new_is_null == old_is_null
                && (
                        datumIsEqual( new_value, old_value, type_byval, type_len )
                     || new_value == old_value
                   )
              )
            {
                // Both fields are NN and are equal
                continue;
            }

            params_count += PARAM_COUNT_PER_ROW;
            params[AUDIT_FIELD_POSITION + index_offset] = datumCopy(
                CStringGetDatum( audit_field ),
                false,
                -2
            );
            params[PK_COLS_POSITION + index_offset] = datumCopy(
                CStringGetDatum( pk_cols ),
                false,
                -2
            );
            params[UID_POSITION + index_offset] = datumCopy(
                CStringGetDatum( uid ),
                false,
                -2
            );
            params[ROW_OP_POSITION + index_offset] = datumCopy(
                CStringGetDatum( update ),
                false,
                -2
            );

            if( old_is_null )
            {
                params[OLD_VALUE_POSITION + index_offset] = PointerGetDatum(
                    NULL
                );
                nulls[OLD_VALUE_POSITION + index_offset] = 'n';
            }
            else
            {
                params[OLD_VALUE_POSITION + index_offset] = datumCopy(
                    old_value,
                    type_byval,
                    type_len
                );
            }

            if( new_is_null )
            {
                params[NEW_VALUE_POSITION + index_offset] = PointerGetDatum(
                    NULL
                );
                nulls[NEW_VALUE_POSITION + index_offset] = 'n';
            }
            else
            {
                params[NEW_VALUE_POSITION + index_offset] = datumCopy(
                    new_value,
                    type_byval,
                    type_len
                );
            }

            param_types[OLD_VALUE_POSITION + index_offset] = typeid;
            param_types[NEW_VALUE_POSITION + index_offset] = typeid;
            index_offset += PARAM_COUNT_PER_ROW;
            row_count++;
        }
    }
    else if( TRIGGER_FIRED_BY_INSERT( trigger_data->tg_event ) )
    {
        return_tuple = trigger_data->tg_trigtuple;
        new          = trigger_data->tg_trigtuple;

        pk_cols = populate_pk_cols(
            num_pk_cols,
            tuple,
            new,
            dat_pk_cols
        );

        if( unlikely( pk_cols == NULL ) )
        {
            SPI_finish();
            return PointerGetDatum( return_tuple );
        }

        for( i = 0; i < num_column_names; i++ )
        {
            column_name = TextDatumGetCString( dat_column_names[i] );
            audit_field = TextDatumGetCString( dat_audit_fields[i] );

            column_num = SPI_fnumber(
                tuple,
                column_name
            );

            if(
                  unlikely(
                      column_num <= 0
                   || column_num == SPI_ERROR_NOATTRIBUTE
                  )
              )
            {
                elog(
                    WARNING,
                    "fn_log_audit_event: Attribute %s not found in record",
                    column_name
                );
                SPI_finish();
                return PointerGetDatum( return_tuple );
            }

            new_value = SPI_getbinval(
                new,
                tuple,
                column_num,
                &new_is_null
            );

            if( new_is_null )
                continue;

            typeid = SPI_gettypeid( tuple, column_num );
            get_typlenbyval( typeid, &type_len, &type_byval );

            params_count += PARAM_COUNT_PER_ROW;
            params[AUDIT_FIELD_POSITION + index_offset] = datumCopy(
                CStringGetDatum( audit_field ),
                false,
                -2
            );
            params[PK_COLS_POSITION + index_offset] = datumCopy(
                CStringGetDatum( pk_cols ),
                false,
                -2
            );
            params[UID_POSITION + index_offset] = datumCopy(
                CStringGetDatum( uid ),
                false,
                -2
            );
            params[ROW_OP_POSITION + index_offset] = datumCopy(
                CStringGetDatum( insert ),
                false,
                -2
            );
            params[OLD_VALUE_POSITION + index_offset] = PointerGetDatum(
                NULL
            );
            params[NEW_VALUE_POSITION + index_offset] = datumCopy(
                new_value,
                type_byval,
                type_len
            );

            nulls[OLD_VALUE_POSITION + index_offset] = 'n';

            param_types[OLD_VALUE_POSITION + index_offset] = typeid;
            param_types[NEW_VALUE_POSITION + index_offset] = typeid;
            index_offset += PARAM_COUNT_PER_ROW;
            row_count++;
        }

    }
    else if( TRIGGER_FIRED_BY_DELETE( trigger_data->tg_event ) )
    {
        return_tuple = trigger_data->tg_trigtuple;
        old          = trigger_data->tg_trigtuple;

        pk_cols = populate_pk_cols(
            num_pk_cols,
            tuple,
            old,
            dat_pk_cols
        );

        if( unlikely( pk_cols == NULL ) )
        {
            SPI_finish();
            return PointerGetDatum( return_tuple );
        }

        for( i = 0; i < num_column_names; i++ )
        {
            column_name = TextDatumGetCString( dat_column_names[i] );
            audit_field = TextDatumGetCString( dat_audit_fields[i] );

            column_num = SPI_fnumber(
                tuple,
                column_name
            );

            if(
                  unlikely(
                      column_num <= 0
                   || column_num == SPI_ERROR_NOATTRIBUTE
                  )
              )
            {
                elog(
                    WARNING,
                    "fn_log_audit_event: Attribute %s not found in record",
                    column_name
                );
                SPI_finish();
                return PointerGetDatum( return_tuple );
            }

            old_value = SPI_getbinval(
                old,
                tuple,
                column_num,
                &old_is_null
            );

            if( old_is_null )
                continue;

            typeid = SPI_gettypeid( tuple, column_num );
            get_typlenbyval( typeid, &type_len, &type_byval );

            params_count += PARAM_COUNT_PER_ROW;
            params[AUDIT_FIELD_POSITION + index_offset] = datumCopy(
                CStringGetDatum( audit_field ),
                false,
                -2
            );
            params[PK_COLS_POSITION + index_offset] = datumCopy(
                CStringGetDatum( pk_cols ),
                false,
                -2
            );
            params[UID_POSITION + index_offset] = datumCopy(
                CStringGetDatum( uid ),
                false,
                -2
            );
            params[ROW_OP_POSITION + index_offset] = datumCopy(
                CStringGetDatum( delete ),
                false,
                -2
            );
            params[OLD_VALUE_POSITION + index_offset] = datumCopy(
                old_value,
                type_byval,
                type_len
            );
            params[NEW_VALUE_POSITION + index_offset] = PointerGetDatum(
                NULL
            );

            nulls[NEW_VALUE_POSITION + index_offset] = 'n';

            param_types[OLD_VALUE_POSITION + index_offset] = typeid;
            param_types[NEW_VALUE_POSITION + index_offset] = typeid;
            index_offset += PARAM_COUNT_PER_ROW;
            row_count++;
        }
    }
    else
    {
        // truncate?
        SPI_finish();
        return PointerGetDatum( get_return_tuple( trigger_data ) );
    }

    if( likely( row_count > 0 ) )
    {
        if( row_count > 1 )
        {
            // Assemble
            for( i = 1; i < row_count; i++ )
            {
                old_query_len = query_len;
                query_len    += (
                                    strlen( audit_event_insert_addl_row )
                                  - ( 2 * PARAM_COUNT_PER_ROW ) // '%u' points
                                  + 1 // NULL term
                                );

                for( j = 0; j < PARAM_COUNT_PER_ROW; j++ )
                {
                    query_len += ( unsigned int )
                                 (
                                    floor(
                                        log10(
                                            param_index + j + 1
                                        )
                                    )
                                  + 1
                                 );
                }

                query = ( char * ) SPI_repalloc(
                    query,
                    sizeof( char ) * ( query_len + 1 )
                );

                if( unlikely( query == NULL ) )
                {
                    elog(
                        WARNING,
                        "fn_log_audit_event: Failed to allocate memory"
                    );

                    SPI_finish();
                    SPI_pfree( params );
                    SPI_pfree( nulls );
                    SPI_pfree( param_types );
                    return PointerGetDatum( return_tuple );
                }

                snprintf(
                    query + old_query_len,
                    query_len - old_query_len,
                    audit_event_insert_addl_row,
                    param_index + 1,
                    param_index + 2,
                    param_index + 3,
                    param_index + 4,
                    param_index + 5,
                    param_index + 6
                );

                query_len = strlen( query );
                param_index += PARAM_COUNT_PER_ROW;
            }
        }

        SPI_execute( set_txid, false, 0 );

        spi_ret = SPI_execute_with_args(
            query,
            params_count,
            param_types,
            params,
            nulls,
            false,
            0
        );

        if( unlikely( spi_ret != SPI_OK_INSERT ) )
            elog( WARNING, "fn_log_audit_event: Audit failure" );

        SPI_freetuptable( SPI_tuptable );
        SPI_tuptable = NULL;
        SPI_pfree( query );
        SPI_pfree( params );
        SPI_pfree( nulls );
        SPI_pfree( param_types );
    }

    if( likely( pk_cols != NULL ) )
        SPI_pfree( pk_cols );

    SPI_finish();
    return PointerGetDatum( return_tuple );
}

static char * populate_pk_cols(
    unsigned int num_pk_cols,
    TupleDesc    tuple,
    HeapTuple    record,
    Datum *      dat
)
{
    char *       pk_cols    = NULL;
    char *       value      = NULL;
    unsigned int i          = 0;
    unsigned int offset     = 0;
    unsigned int size       = 0;
    int          column_num = 0;

    size = strlen( pk_cols_prefix ) + 1;
    pk_cols = ( char * ) SPI_palloc(
        sizeof( char ) * size
    );

    if( unlikely( pk_cols == NULL ) )
    {
        elog(
            WARNING,
            "fn_log_audit_event: Failed to allocate memory"
        );
        return NULL;
    }

    strncpy( pk_cols, pk_cols_prefix, strlen( pk_cols_prefix ) );
    pk_cols[strlen( pk_cols_prefix )] = '\0';

    if( num_pk_cols > 1 )
    {
        offset = 1;
    }

    for( i = 0; i < num_pk_cols; i++ )
    {
        if( i == num_pk_cols - 1 && offset == 1 )
        {
            offset = 0;
        }

        column_num = SPI_fnumber(
            tuple,
            ( const char * ) TextDatumGetCString( dat[i] )
        );

        if( unlikely( column_num == SPI_ERROR_NOATTRIBUTE ) )
        {
            elog(
                WARNING,
                "fn_log_audit_event: Invalid pk_column "
                "with attribute index %d (%s)",
                column_num,
                TextDatumGetCString( dat[i] )
            );
            return NULL;
        }

        value = SPI_getvalue(
            record,
            tuple,
            column_num
        );

        size   += strlen( value ) + offset + 1;
        pk_cols = ( char * ) SPI_repalloc(
            pk_cols,
            sizeof( char ) * size
        );

        if( unlikely( pk_cols == NULL ) )
        {
            elog(
                WARNING,
                "fn_log_audit_event: Failed to allocate memory"
            );
            return NULL;
        }

        strncat( pk_cols, value, strlen( value ) );

        if( offset == 1 )
        {
            strncat( pk_cols, ",", 1 );
        }

        pk_cols[size-1] = '\0';
    }

    size += strlen( pk_cols_suffix );
    pk_cols = SPI_repalloc(
        pk_cols,
        sizeof( char ) * size
    );

    if( unlikely( pk_cols == NULL ) )
    {
        elog(
            WARNING,
            "fn_log_audit_event: Failed to allocate memory"
        );
        return NULL;
    }

    strncat( pk_cols, pk_cols_suffix, strlen( pk_cols_suffix ) );
    pk_cols[size-1] = '\0';
    return pk_cols;
}

static Datum * parse_input_array( char * string, int * elements )
{
    Datum        raw_value          = {0};
    Datum *      array_ret          = NULL;
    ArrayType *  array_intermediate = NULL;
    unsigned int size               = 0;
    char *       val                = NULL;

    // We expect strings to come in like:
    // {val1,val2,...}
    // We need to strip the curlies off, since postgres' native representation
    // is in that form but it isn't parsable by exported functions
    if( unlikely( string == NULL || elements == NULL ) )
        return NULL;

    size = strlen( string );
    val  = ( char * ) SPI_palloc( sizeof( char ) * size );

    if( unlikely( val == NULL ) )
        return NULL;

    strncpy( val, string, size );

    if( size > 1 )
    {
        val[size-1] = '\0'; // blast over the last curly
        size--;
    }

    // Shift the string left by 1 by incrementing the pointer
    raw_value = datumCopy( CStringGetTextDatum( val + 1 ), true, -2 );
    SPI_pfree( val );
    array_intermediate = DatumGetArrayTypeP(
        DirectFunctionCall2(
            text_to_array,
            raw_value,
            CStringGetTextDatum( "," )
        )
    );

    deconstruct_array(
        array_intermediate,
        TEXTOID,
        -1,
        false,
        'i',
        &array_ret,
        NULL,
        elements
    );

    return array_ret;
}

// get the logging UID. This function avoids calls to
// fn_get_uid_by_username and fn_get_current_uid, shaving ~4ms off
static char * get_current_uid( void )
{
    HeapTuple       htup          = NULL;
    SPITupleTable * tuptable      = NULL;
    Datum           param[1]      = {0};
    Oid             param_type[1] = {0};
    char *          uid           = NULL;
    char *          user_table    = NULL;
    char *          username_col  = NULL;
    char *          uid_col       = NULL;
    char *          uid_lookup_q  = NULL;
    char            nulls[2]      = {0};
    int             spi_ret       = 0;
    Datum           currentuser   = 0;
    unsigned int    size          = 0;

    uid = ( char * ) GetConfigOption(
        "cyanaudit.uid",
        true,
        false
    );

    if( uid == NULL || uid[0] == '-' )
    {
        param_type[0] = CSTRINGOID;
        nulls[0]      = ' ';

        // Progressively lookup and fill uid_col, username_col, and user_table
        // from cyanaudit.tb_config, then snprintf into uid_lookup query
        // to perform the lookup. If there's any trouble we bailout and set
        // the UID to 0
        param[0] = CStringGetDatum( "user_table" );
        spi_ret = SPI_execute_with_args(
            config_lookup,
            1,
            param_type,
            param,
            nulls,
            true,
            1
        );

        if( likely( SPI_tuptable != NULL && spi_ret == SPI_OK_SELECT ) )
        {
            tuptable = SPI_tuptable;
            htup = tuptable->vals[0];

            if( likely( htup != NULL ) )
            {
                user_table = SPI_getvalue(
                    htup,
                    SPI_tuptable->tupdesc,
                    1
                );
            }
            else
            {
                set_default_uid();
                SPI_freetuptable( SPI_tuptable );
                SPI_tuptable = NULL;
                return "0";
            }

            SPI_freetuptable( SPI_tuptable );
            SPI_tuptable = NULL;
        }
        else
        {
            set_default_uid();
            return "0";
        }

        param[0] = CStringGetDatum( "user_table_uid_col" );
        spi_ret = SPI_execute_with_args(
            config_lookup,
            1,
            param_type,
            param,
            nulls,
            true,
            1
        );

        if( likely( SPI_tuptable != NULL && spi_ret == SPI_OK_SELECT ) )
        {
            tuptable = SPI_tuptable;
            htup = tuptable->vals[0];

            if( likely( htup != NULL ) )
            {
                uid_col = SPI_getvalue(
                    htup,
                    SPI_tuptable->tupdesc,
                    1
                );
            }
            else
            {
                set_default_uid();
                SPI_freetuptable( SPI_tuptable );
                SPI_tuptable = NULL;
                return "0";
            }

            SPI_freetuptable( SPI_tuptable );
            SPI_tuptable = NULL;
        }
        else
        {
            set_default_uid();
            return "0";
        }

        param[0] = CStringGetDatum( "user_table_username_col" );
        spi_ret = SPI_execute_with_args(
            config_lookup,
            1,
            param_type,
            param,
            nulls,
            true,
            1
        );

        if( likely( SPI_tuptable != NULL && spi_ret == SPI_OK_SELECT ) )
        {
            tuptable = SPI_tuptable;
            htup = tuptable->vals[0];

            if( likely( htup != NULL ) )
            {
                username_col = SPI_getvalue(
                    htup,
                    SPI_tuptable->tupdesc,
                    1
                );
            }
            else
            {
                set_default_uid();
                SPI_freetuptable( SPI_tuptable );
                SPI_tuptable = NULL;
                return "0";
            }

            SPI_freetuptable( SPI_tuptable );
            SPI_tuptable = NULL;
        }
        else
        {
            set_default_uid();
            return "0";
        }

        if(
            unlikely(
                username_col == NULL
             || uid_col == NULL
             ||  user_table == NULL
            )
          )
        {
            set_default_uid();
            return "0";
        }

        size = strlen( uid_lookup )
             - 6
             + strlen( username_col )
             + strlen( uid_col )
             + strlen( user_table )
             + 1;

        uid_lookup_q = ( char * ) SPI_palloc(
            sizeof( char ) * ( size + 1 )
        );

        if( unlikely( uid_lookup_q == NULL ) )
        {
            set_default_uid();
            return "0";
        }

        snprintf(
            uid_lookup_q,
            size,
            uid_lookup,
            uid_col,
            user_table,
            username_col
        );

        uid_lookup_q[size] = '\0';
        currentuser = DirectFunctionCall2(
            current_user,
            0,
            0
        );

        param[0] = currentuser;
        param_type[0] = NAMEOID;

        spi_ret = SPI_execute_with_args(
            uid_lookup_q,
            1,
            param_type,
            param,
            nulls,
            true,
            1
        );

        if( likely( SPI_tuptable != NULL && spi_ret == SPI_OK_SELECT ) )
        {
            tuptable = SPI_tuptable;
            htup = tuptable->vals[0];

            if( likely( htup != NULL ) )
            {
                uid = SPI_getvalue(
                    htup,
                    SPI_tuptable->tupdesc,
                    1
                );

                SetConfigOption(
                    "cyanaudit.uid",
                    uid,
                    ( superuser() ? PGC_SUSET : PGC_USERSET ),
                    PGC_S_SESSION
                );
            }
            else
            {
                set_default_uid();
                SPI_freetuptable( SPI_tuptable );
                SPI_tuptable = NULL;
                return "0";
            }

            SPI_freetuptable( SPI_tuptable );
            SPI_tuptable = NULL;
        }
        else
        {
            set_default_uid();
            return "0";
        }
    }

    return uid;
}

static void set_default_uid( void )
{
    SetConfigOption(
        "cyanaudit.uid",
        "0",
        ( superuser() ? PGC_SUSET : PGC_USERSET ),
        PGC_S_SESSION
    );

    return;
}

static inline Datum get_return_tuple( TriggerData * trigger_data )
{
    if( unlikely( trigger_data == NULL ) )
        return (Datum) 0;

    if( TRIGGER_FIRED_BY_UPDATE( trigger_data->tg_event ) )
        return PointerGetDatum( trigger_data->tg_newtuple );

    return PointerGetDatum( trigger_data->tg_trigtuple );
}
