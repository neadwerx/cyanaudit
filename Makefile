EXTENSION		= cyanaudit
EXTVERSION      = $(shell grep default_version $(EXTENSION).control | \
                  sed -e "s/default_version[[:space:]]*=[[:space:]]*'\([^']*\)'/\1/" )
DOCS            = README.md
DEBUG			= -g -DDEBUG
PG_CONFIG		?= pg_config
PGLIBDIR		= $(shell $(PG_CONFIG) --libdir)
PGINCLUDEDIR	= $(shell $(PG_CONFIG) --includedir-server)
PGXS			= $(shell $(PG_CONFIG) --pgxs)
CC				= gcc
SRCS			= $(wildcard src/*.c)
OBJS			= $(SRCS:.c=.o)
PG_CFLAGS		= -I$(PGINCLUDEDIR) $(DEBUG)
EXTRA_CLEAN		= src/*.o src/*.so src/*.bc *.o *.so *.bc
MODULE_big 		= $(EXTENSION)

include $(PGXS)
