BEGIN;

do language plpgsql
 $$
declare
    my_value            varchar;
    my_version          integer[];
    my_command          varchar;
begin
    my_version := regexp_matches(version(), 'PostgreSQL (\d+)\.(\d+)');

    -- Verify minimum version
    if my_version < array[10,0]::integer[] then
        raise exception 'Cyan Audit requires PostgreSQL 10 or above';
    end if;
end;
 $$;

-- fn_get_email_by_uid
CREATE OR REPLACE FUNCTION cyanaudit.fn_get_email_by_uid
(
    in_uid  integer
)
returns varchar
language plpgsql stable strict
as $_$
declare
    my_email                varchar;
    my_query                varchar;
    my_user_table_uid_col   varchar;
    my_user_table           varchar;
    my_user_table_email_col varchar;
begin
    PERFORM *
       FROM pg_class c
       JOIN pg_namespace n
         ON n.nspname = 'cyanaudit'
        AND n.oid = c.relnamespace
        AND c.relname = 'tb_config';

    IF NOT FOUND THEN
        RETURN NULL;
    END IF;

    select value
      into my_user_table
      from cyanaudit.tb_config
     where name = 'user_table';

    select value
      into my_user_table_uid_col
      from cyanaudit.tb_config
     where name = 'user_table_uid_col';

    select value
      into my_user_table_email_col
      from cyanaudit.tb_config
     where name = 'user_table_email_col';

    if my_user_table            IS NULL OR
       my_user_table_uid_col    IS NULL OR
       my_user_table_email_col  IS NULL
    then
        return null;
    end if;

    my_query := 'SELECT ' || quote_ident(my_user_table_email_col)
             || '  FROM ' || quote_ident(my_user_table)
             || ' WHERE ' || quote_ident(my_user_table_uid_col) || ' = ' || quote_nullable(in_uid);

    PERFORM c.oid
       FROM pg_class c
       JOIN pg_attribute auid
         ON auid.attrelid = c.oid
        AND auid.attnum > 0
        AND auid.attisdropped IS FALSE
        AND auid.attname = my_user_table_uid_col
       JOIN pg_attribute aem
         ON aem.attrelid = c.oid
        AND aem.attnum > 0
        AND aem.attisdropped IS FALSE
        AND aem.attname = my_user_table_email_col
      WHERE c.relname = my_user_table;

    IF NOT FOUND OR my_query IS NULL THEN
        raise notice 'cyanaudit: Invalid user_table setting: ''%'' or invalid column ''%'' or ''%''',
            my_user_table,
            my_user_table_uid_col,
            my_user_table_email_col;
        RETURN NULL;
    END IF;

    execute my_query
       into my_email;

    return my_email;
end
 $_$;

-- fn_get_uid_by_username
CREATE OR REPLACE FUNCTION cyanaudit.fn_get_uid_by_username
(
    in_username varchar
)
returns integer
language plpgsql stable strict
as $_$
declare
    my_uid                      varchar;
    my_query                    varchar;
    my_user_table_uid_col       varchar;
    my_user_table               varchar;
    my_user_table_username_col  varchar;
begin
    select value
      into my_user_table
      from cyanaudit.tb_config
     where name = 'user_table';

    PERFORM *
       FROM pg_class c
       JOIN pg_namespace n
         ON n.nspname = 'cyanaudit'
        AND n.oid = c.relnamespace
        AND c.relname = 'tb_config';

    IF NOT FOUND THEN
        RETURN NULL;
    END IF;

    select value
      into my_user_table_uid_col
      from cyanaudit.tb_config
     where name = 'user_table_uid_col';

    select value
      into my_user_table_username_col
      from cyanaudit.tb_config
     where name = 'user_table_username_col';

    if my_user_table                IS NULL OR
       my_user_table_uid_col        IS NULL OR
       my_user_table_username_col   IS NULL
    then
        return null;
    end if;

    my_query := 'select ' || quote_ident(my_user_table_uid_col)
             || '  from ' || quote_ident(my_user_table)
             || ' where ' || quote_ident(my_user_table_username_col)
                          || ' = ' || quote_nullable(in_username);
    PERFORM c.oid
       FROM pg_class c
       JOIN pg_attribute auid
         ON auid.attrelid = c.oid
        AND auid.attnum > 0
        AND auid.attisdropped IS FALSE
        AND auid.attname = my_user_table_uid_col
       JOIN pg_attribute aem
         ON aem.attrelid = c.oid
        AND aem.attnum > 0
        AND aem.attisdropped IS FALSE
        AND aem.attname = my_user_table_username_col
      WHERE c.relname = my_user_table;

    IF NOT FOUND OR my_query IS NULL THEN
        raise notice 'cyanaudit: Invalid user_table setting: ''%'' or invalid column ''%'' or ''%''',
            my_user_table,
            my_user_table_uid_col,
            my_user_table_username_col;
        RETURN NULL;
    END IF;

    execute my_query
       into my_uid;

    return my_uid;
end
 $_$;

-- fn_log_audit_event (MAIN LOGGING TRIGGER FUNCTION)
CREATE OR REPLACE FUNCTION cyanaudit.fn_log_audit_event()
 RETURNS trigger
 LANGUAGE plpgsql
AS $_$
declare
    my_audit_fields         varchar[];
    my_column_names         varchar[];
    my_record               RECORD;
    my_pk_cols              varchar;
    my_pk_vals_constructor  varchar;
    my_pk_vals              varchar[];
    my_clock_timestamp      timestamp;
    my_suffix               TEXT;
    my_select               TEXT; 
begin
    if( TG_OP = 'INSERT' ) then
        my_record           := NEW;
        my_select           := 'NULL::TEXT AS old_val, n.value AS new_val,';
        my_suffix           := '  JOIN jsonb_each_text( ' || quote_literal( to_jsonb( NEW.* )::TEXT ) || ' ) n ON n.key = a.columns'
                            || ' WHERE n.value IS NOT NULL';
    elsif( TG_OP = 'UPDATE' ) then
        my_record           := NEW;
        my_select           := 'o.value AS old_val, n.value AS new_val,';
        my_suffix           := '  JOIN jsonb_each_text( ' || quote_literal( to_jsonb( NEW.* )::TEXT ) || ' ) n ON n.key = a.columns'
                            || '  JOIN jsonb_each_text( ' || quote_literal( to_jsonb( OLD.* )::TEXT ) || ' ) o ON o.key = a.columns'
                            || ' WHERE n.value IS DISTINCT FROM o.value';
    elsif( TG_OP = 'DELETE' ) then
        my_record           := OLD;
        my_select           := 'o.value AS old_val, NULL::TEXT AS new_val,';
        my_suffix           := '  JOIN jsonb_each_text( ' || quote_literal( to_jsonb( OLD.* )::TEXT ) || '::JSONB ) o ON o.key = a.columns'
                            || ' WHERE o.value IS NOT NULL';
    end if;

    IF( current_setting( 'cyanaudit.enabled', TRUE ) = ANY( ARRAY[ '0', 'false', 'f' ]::VARCHAR[] ) ) THEN
        RETURN my_record;
    END IF;

    -- Pre-flight checks
    -- Checks that the function we call when logging exists
    PERFORM p.oid
       FROM pg_proc p
       JOIN pg_namespace n
         ON n.oid = p.pronamespace
        AND n.nspname = 'cyanaudit'
      WHERE p.proname = 'fn_get_current_uid';

    IF NOT FOUND THEN
        RAISE NOTICE 'Cyanaudit: Operation not logged. Missing function - please reinstall Cyanaudit';
        RETURN my_record;
    END IF;

    my_pk_cols          := TG_ARGV[0]::varchar[];
    my_audit_fields     := TG_ARGV[1]::varchar[];
    my_column_names     := TG_ARGV[2]::varchar[];

    -- Checks that the attributes we are going to fetch from the psuedorecord should exist.
    PERFORM c.oid
       FROM pg_class c
       JOIN pg_attribute a
         ON a.attrelid = c.oid
        AND a.attnum > 0
        AND a.attisdropped IS FALSE
      WHERE c.oid = TG_RELID
   GROUP BY c.oid
     HAVING array_agg( DISTINCT a.attname )::VARCHAR[] @> my_column_names::VARCHAR[]
        AND array_agg( DISTINCT a.attname )::VARCHAR[] @> my_pk_cols::VARCHAR[];

    IF NOT FOUND THEN
        RAISE NOTICE 'Cyanaudit: Operation not logged. Bad logging trigger arguments - please reinstall Cyanaudit';
        RETURN my_record;
    END IF;

    my_clock_timestamp  := clock_timestamp(); -- same for all entries from this invocation

    -- Bookmark this txid in cyanaudit.last_txid
    perform (set_config('cyanaudit.last_txid', txid_current()::text, false))::bigint;

    -- Given:  my_pk_cols::varchar[]           = ARRAY[ 'column foo',bar ]
    -- Result: my_pk_vals_constructor::varchar = 'select ARRAY[ $1."column foo", $1.bar ]::varchar[]'
    select 'SELECT ARRAY[' || string_agg( '$1.' || quote_ident(pk_col), ',' ) || ']::varchar[]'
      into my_pk_vals_constructor
      from ( select unnest(my_pk_cols::varchar[]) as pk_col ) x;

    -- Execute the result using my_record in $1 to produce the following result:
    -- my_pk_vals::varchar[] = ARRAY[ 'val1', 'val2' ]
    execute my_pk_vals_constructor
       into my_pk_vals
      using my_record; -- To allow undoing updates to pk columns, logged pk_vals are post-update.

    EXECUTE 'WITH tt_audit AS
    (
        SELECT unnest( $1 ) AS columns,
               unnest( $2 ) AS audit_fields
    ),
    tt_audit_values AS
    (
        SELECT a.columns,
               a.audit_fields::INTEGER,
          ' || my_select || '
               CASE WHEN current_setting( ''cyanaudit.audit_transaction_type'', TRUE ) ~ ''^\d+$''
                    THEN current_setting( ''cyanaudit.audit_transaction_type'', TRUE )::INTEGER
                    ELSE NULL::INTEGER
                     END AS audit_transaction_type,
               cyanaudit.fn_get_current_uid() AS current_uid
          FROM tt_audit a
          ' || my_suffix || '
    )
        INSERT INTO cyanaudit.tb_audit_event
                    (
                        audit_field,
                        recorded,
                        pk_vals,
                        uid,
                        row_op,
                        audit_transaction_type,
                        old_value,
                        new_value
                    )
             SELECT av.audit_fields,
               ''' || my_clock_timestamp || '''::TIMESTAMP,
                    $3,
                    av.current_uid,
               ''' || TG_OP::CHAR(1) || ''',
                    av.audit_transaction_type,
                    old_val,
                    new_val
               FROM tt_audit_values av'
      USING my_column_names::VARCHAR[],
            my_audit_fields::VARCHAR[],
            my_pk_vals;

    return my_record;
end
$_$;

-- EVENT TRIGGER
-- fn_update_audit_fields_event_trigger()
CREATE OR REPLACE FUNCTION cyanaudit.fn_update_audit_fields_event_trigger()
returns event_trigger
language plpgsql as
   $function$
declare
    my_command_tag  varchar;
    my_object_schema varchar;
    my_dropped_object record;
begin
    -- Avoid creating logging triggers during database restore,
    -- since the triggers will be restored without our help.
    perform *
       from pg_stat_activity
      where application_name = 'pg_restore'
        and datname = current_database()
        and state = 'active';

    if found then
        return;
    end if;

    IF TG_EVENT = 'ddl_command_end' AND TG_TAG IN ('ALTER TABLE', 'CREATE TABLE') THEN
        PERFORM * FROM pg_event_trigger_ddl_commands();

        -- Can happen during 'CREATE TABLE IF NOT EXISTS'
        IF NOT FOUND THEN
            RETURN;
        END IF;

        FOR my_command_tag, my_object_schema IN SELECT command_tag, schema_name FROM pg_event_trigger_ddl_commands() LOOP
            IF my_object_schema = 'pg_temp' THEN
                RETURN;
            END IF;
        END LOOP;
    ELSIF TG_EVENT = 'sql_drop' AND TG_TAG = 'DROP TABLE' THEN
        PERFORM * FROM pg_event_trigger_dropped_objects();

        IF NOT FOUND THEN
            RETURN;
        END IF;

        FOR my_dropped_object IN SELECT * FROM pg_event_trigger_dropped_objects() LOOP
            IF my_dropped_object.is_temporary THEN
                RETURN;
            END IF;
        END LOOP;
    ELSE
        -- We only care about ALTER TABLE, CREATE TABLE, and DROP TABLE



-- fn_create_event_trigger
-- Function to install the event trigger explicitly after pg_restore completes,
-- because we don't want it firing during pg_restore.
CREATE OR REPLACE FUNCTION cyanaudit.fn_create_event_trigger()
RETURNS void
LANGUAGE plpgsql
AS $_$
begin
    -- drop the deprecated event trigger
    PERFORM *
       from pg_event_trigger
      where evtname = 'tr_update_audit_fields';

    if found then
        DROP EVENT TRIGGER tr_update_audit_fields;
    end if;

    PERFORM *
       from pg_event_trigger
      where evtname = 'tr_update_audit_fields_on_create_update_table';

    if not found then
        CREATE EVENT TRIGGER tr_update_audit_fields_on_create_update_table ON ddl_command_end
            WHEN TAG IN ('ALTER TABLE', 'CREATE TABLE')
            EXECUTE PROCEDURE cyanaudit.fn_update_audit_fields_event_trigger();
    end if;

    PERFORM *
       from pg_event_trigger
      where evtname = 'tr_update_audit_fields_on_drop_table';

    if not found then
        CREATE EVENT TRIGGER tr_update_audit_fields_on_drop_table ON sql_drop
            WHEN TAG IN ('DROP TABLE')
            EXECUTE PROCEDURE cyanaudit.fn_update_audit_fields_event_trigger();
    end if;
end;
 $_$;

INSERT INTO cyanaudit.tb_config (name, value)
     VALUES ('version', '2.2.2')
ON CONFLICT (name) DO UPDATE
        SET value = EXCLUDED.value
      WHERE cyanaudit.tb_config.name = EXCLUDED.name;

COMMIT;
