BEGIN;
INSERT INTO cyanaudit.tb_config
            (
                name,
                value
            )
     SELECT 'active_partition',
            c.relname
       FROM pg_class c
 INNER JOIN pg_namespace n
         ON n.oid = c.relnamespace
        AND n.nspname = 'cyanaudit'
      WHERE c.relname ILIKE 'tb_audit_event_%'
   ORDER BY c.relname DESC
      LIMIT 1
ON CONFLICT (name) DO UPDATE
        SET value = EXCLUDED.value
      WHERE cyanaudit.tb_config.name = EXCLUDED.name;

CREATE OR REPLACE FUNCTION cyanaudit.fn_log_audit_event()
 RETURNS trigger
 LANGUAGE plpgsql
AS $_$
declare
    my_record               RECORD;
    my_pk_vals_constructor  varchar;
    my_partition            varchar;
    my_suffix               TEXT;
    my_select               TEXT;
begin
    SELECT cf.value
      INTO my_partition
      FROM cyanaudit.tb_config cf
     WHERE cf.name = 'active_partition';

    IF( current_setting( 'cyanaudit.enabled', TRUE ) = ANY( ARRAY[ '0', 'false', 'f' ]::VARCHAR[] ) OR my_partition IS NULL ) THEN
        IF( my_partition IS NULL ) THEN
            RAISE NOTICE 'Cyanaudit: Operation not logged. No active partition - please reinstall Cyanaudit';
        END IF;
        RETURN my_record;
    END IF;

    if( TG_OP = 'INSERT' ) then
        my_record           := NEW;
        my_select           := 'NULL::TEXT AS old_val, n.value AS new_val,';
        my_suffix           := '  JOIN jsonb_each_text( ' || quote_literal( to_jsonb( NEW.* )::TEXT ) || ' ) n ON n.key = a.columns'
                            || ' WHERE n.value IS NOT NULL';
    elsif( TG_OP = 'UPDATE' ) then
        my_record           := NEW;
        my_select           := 'o.value AS old_val, n.value AS new_val,';
        my_suffix           := '  JOIN jsonb_each_text( ' || quote_literal( to_jsonb( NEW.* )::TEXT ) || ' ) n ON n.key = a.columns'
                            || '  JOIN jsonb_each_text( ' || quote_literal( to_jsonb( OLD.* )::TEXT ) || ' ) o ON o.key = a.columns'
                            || ' WHERE n.value IS DISTINCT FROM o.value';
    elsif( TG_OP = 'DELETE' ) then
        my_record           := OLD;
        my_select           := 'o.value AS old_val, NULL::TEXT AS new_val,';
        my_suffix           := '  JOIN jsonb_each_text( ' || quote_literal( to_jsonb( OLD.* )::TEXT ) || '::JSONB ) o ON o.key = a.columns'
                            || ' WHERE o.value IS NOT NULL';
    end if;

    -- Given:  my_pk_cols::varchar[]           = ARRAY[ 'column foo',bar ]
    -- Result: my_pk_vals_constructor::varchar = 'select ARRAY[ $1."column foo", $1.bar ]::varchar[]'
    select 'SELECT ARRAY[' || string_agg( '$3.' || quote_ident(pk_col), ',' ) || ']::varchar[]'
      into my_pk_vals_constructor
      from ( select unnest(TG_ARGV[0]::varchar[]) as pk_col ) x;

    EXECUTE 'WITH tt_audit AS
    (
        SELECT unnest( $1 ) AS columns,
               unnest( $2 ) AS audit_fields,
               clock_timestamp() AS clock_timestamp,
    ),
    tt_audit_values AS
    (
        SELECT a.columns,
               a.audit_fields::INTEGER,
               a.clock_timestamp::TIMESTAMP,
          ' || my_select || '
               CASE WHEN current_setting( ''cyanaudit.audit_transaction_type'', TRUE ) ~ ''^\d+$''
                    THEN current_setting( ''cyanaudit.audit_transaction_type'', TRUE )::INTEGER
                    ELSE NULL::INTEGER
                     END AS audit_transaction_type,
               COALESCE(
                   NULLIF(
                       current_setting(
                           ''cyanaudit.uid'',
                           TRUE
                       ),
                       ''''
                   )::TEXT,
                   set_config(
                       ''cyanaudit.uid''::TEXT,
                       COALESCE(
                           cyanaudit.fn_get_uid_by_username( current_user::VARCHAR )::TEXT,
                           0::TEXT
                        ),
                        FALSE
                   )::TEXT
               )::INTEGER AS current_uid
          FROM tt_audit a
          ' || my_suffix || '
    )
        INSERT INTO cyanaudit.' || my_partition || '
                    (
                        audit_field,
                        recorded,
                        pk_vals,
                        uid,
                        row_op,
                        txid,
                        audit_transaction_type,
                        old_value,
                        new_value
                    )
             SELECT av.audit_fields,
                    av.clock_timestamp,
               ( ' || my_pk_vals_constructor || ' ),
                    av.current_uid,
                    $4,
                    set_config( ''cyanaudit.last_txid'', txid_current()::TEXT, FALSE )::BIGINT,
                    av.audit_transaction_type,
                    old_val,
                    new_val
               FROM tt_audit_values av'
      USING TG_ARGV[2]::VARCHAR[],
            TG_ARGV[1]::VARCHAR[],
            my_record,
            TG_OP::CHAR(1);

    return my_record;
end
$_$;

DROP TRIGGER tr_redirect_audit_events ON cyanaudit.tb_audit_event;
DROP FUNCTION cyanaudit.fn_redirect_audit_events();


CREATE OR REPLACE FUNCTION cyanaudit.fn_get_active_partition_name()
returns varchar as
 $_$
    SELECT c.relname
      FROM pg_class c
      JOIN cyanaudit.tb_config cf
        ON cf.value = c.relname
       AND cf.name = 'active_partition'
      JOIN pg_namespace n
        ON n.nspname::VARCHAR = 'cyanaudit'
       AND n.oid = c.relnamespace;
 $_$
    language sql stable parallel safe;

CREATE OR REPLACE FUNCTION cyanaudit.fn_activate_partition
(
    in_partition_name   varchar
)
returns void as
 $_$
declare
    my_active_partition_name   varchar;
begin
    if in_partition_name is null then
        raise exception 'in_partition_name cannot be null';
    end if;

    my_active_partition_name := cyanaudit.fn_get_active_partition_name();

    if my_active_partition_name = in_partition_name then
        -- already configured correctly
        return;
    end if;

    UPDATE cyanaudit.tb_config
       SET value = in_partition_name
     WHERE value = my_active_partition_name
       AND name = 'active_partition';
    return;
end
 $_$
    language plpgsql;

CREATE OR REPLACE FUNCTION cyanaudit.fn_enforce_valid_active_partition()
RETURNS TRIGGER AS
 $_$
BEGIN
    IF( ( TG_OP = 'INSERT' ) OR ( TG_OP = 'UPDATE' ) ) THEN
        IF( NEW.name != 'active_partition' ) THEN
            RETURN NEW;
        END IF;
    ELSIF( TG_OP = 'DELETE' ) THEN
        IF( OLD.name = 'active_partition' ) THEN
            RAISE NOTICE 'Cannot delete active_partition key - critical to logging';
            RETURN NULL;
        END IF;
        RETURN OLD;
    END IF;

    PERFORM c.relname
       FROM pg_class c
       JOIN pg_namespace n
         ON n.nspname = 'cyanaudit'
        AND n.oid = c.relnamespace
       JOIN pg_inherits i
         ON i.inhrelid = c.oid
       JOIN pg_class cp
         ON cp.relnamespace = n.oid
        AND cp.oid = i.inhparent
        AND cp.relname = 'tb_audit_event'
        AND cp.relkind = 'r'
      WHERE c.relname = NEW.value
        AND c.relkind = 'r'
        AND c.relname ILIKE 'tb_audit_event_%';

    IF NOT FOUND THEN
        RAISE NOTICE '% does not inherit tb_audit_event, has invalid name, or does not exist.', NEW.value;
        RETURN NULL;
    END IF;

    RETURN NEW;
END
 $_$
    LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS tr_before_active_partition_change ON cyanaudit.tb_config;
CREATE TRIGGER tr_before_active_partition_change
    BEFORE INSERT OR UPDATE OR DELETE ON cyanaudit.tb_config
    FOR EACH ROW EXECUTE PROCEDURE cyanaudit.fn_enforce_valid_active_partition();

CREATE OR REPLACE FUNCTION cyanaudit.fn_protect_active_partition()
RETURNS event_trigger
AS
 $_$
BEGIN
    IF( TG_EVENT = 'sql_drop' AND TG_TAG IN( 'DROP TABLE' ) ) THEN
        PERFORM c.relname
           FROM pg_event_trigger_dropped_objects() dr
     INNER JOIN cyanaudit.tb_config co
             ON co.value = dr.object_name
            AND co.name = 'active_partition'
          WHERE dr.is_temporary IS FALSE
            AND dr.schema_name = 'cyanaudit';

        IF FOUND THEN
            RAISE EXCEPTION 'Cannot drop active partition!';
            RETURN;
        END IF;
    END IF;

    RETURN;
END
 $_$
    LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION cyanaudit.fn_create_event_trigger()
RETURNS void
LANGUAGE plpgsql
AS $_$
begin
    -- drop the deprecated event trigger
    PERFORM *
       from pg_event_trigger
      where evtname = 'tr_update_audit_fields';

    if found then
        DROP EVENT TRIGGER tr_update_audit_fields;
    end if;

    PERFORM *
       from pg_event_trigger
      where evtname = 'tr_update_audit_fields_on_create_update_table';

    if not found then
        CREATE EVENT TRIGGER tr_update_audit_fields_on_create_update_table ON ddl_command_end
            WHEN TAG IN ('ALTER TABLE', 'CREATE TABLE')
            EXECUTE PROCEDURE cyanaudit.fn_update_audit_fields_event_trigger();
    end if;

    PERFORM *
       from pg_event_trigger
      where evtname = 'tr_update_audit_fields_on_drop_table';

    if not found then
        CREATE EVENT TRIGGER tr_update_audit_fields_on_drop_table ON sql_drop
            WHEN TAG IN ('DROP TABLE')
            EXECUTE PROCEDURE cyanaudit.fn_update_audit_fields_event_trigger();
    end if;

    PERFORM *
       FROM pg_event_trigger
      WHERE evtname = 'tr_protect_active_partition';

    IF NOT FOUND THEN
        CREATE EVENT TRIGGER tr_protect_active_partition ON sql_drop
            WHEN TAG IN( 'DROP TABLE' )
            EXECUTE PROCEDURE cyanaudit.fn_protect_active_partition();
    END IF;
end;
 $_$;

DROP FUNCTION IF EXISTS cyanaudit.fn_parse_tgargs( BYTEA );
INSERT INTO cyanaudit.tb_config( name, value )
     VALUES ('version', '2.2.3')
ON CONFLICT (name) DO UPDATE
        SET value = EXCLUDED.value
      WHERE cyanaudit.tb_config.name = EXCLUDED.name;

WITH tt_latest_partition AS
(
    SELECT c.relname::VARCHAR AS active_partition
      FROM pg_class c
INNER JOIN pg_namespace n
        ON n.oid = c.relnamespace
       AND n.nspname::VARCHAR = 'cyanaudit'
     WHERE c.relname ILIKE 'tb_audit_event_%'
  ORDER BY c.relname DESC
     LIMIT 1
)
INSERT INTO cyanaudit.tb_config
            (
                name,
                value
            )
     SELECT 'active_partition',
            tt.active_partition
       FROM tt_latest_partition tt
ON CONFLICT (name) DO UPDATE
        SET value = EXCLUDED.value
      WHERE cyanaudit.tb_config.name = EXCLUDED.name;

DROP FUNCTION IF EXISTS cyanaudit.fn_get_current_uid();
SELECT cyanaudit.fn_create_event_trigger(); 
COMMIT;
